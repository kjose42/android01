package edu.rutgers.model;

import android.app.Activity;
import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class AlbumsData extends Activity implements Serializable {
    static final long serialVersionUID = 1L;
    private static final String albumsPath = "user.dat";

    protected static void writeAlbums(User user, Context ctx) throws IOException {
        FileOutputStream fileOut = ctx.openFileOutput(albumsPath, MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fileOut);
        oos.writeObject(user);
    }

    public static User getAlbums(Context ctx) throws IOException {
        User user = new User(ctx);
        try {
            FileInputStream fileIn = ctx.openFileInput(albumsPath);
            ObjectInputStream ois = new ObjectInputStream(fileIn);
            user = (User)ois.readObject();
            user.setContext(ctx);
            ois.close();
        } catch (IOException | ClassNotFoundException e ) {
            writeAlbums(user, ctx);
            e.printStackTrace();
        }
        return user;
    }
}
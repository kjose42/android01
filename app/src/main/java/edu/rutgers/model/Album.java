package edu.rutgers.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Album implements Serializable {
    static final long serialVersionUID = 3L;
    private String name;
    private final List<Photo> photos;
    private User user;

    public Album(String name, User user) {
        this.name = name.trim();
        this.photos = new ArrayList<>();
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public boolean add(Photo p) {
        try {
            for (Album a : user.getAlbums()) {
                if (a.getPhotos().contains(p)) {
                    return false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        photos.add(p);
        updateAlbums();

        return true;
    }

    public void delete(Photo p) {
        if (photos.contains(p)) {
            photos.remove(p);
            if (p.getFile().exists()) {
                p.getFile().delete();
            }
            updateAlbums();
        }
    }

    public void move(Photo p, Album a) {
        try {
            int albumIndex = user.getAlbums().indexOf(a);
            if (albumIndex != -1) {
                photos.remove(p);
                p.setAlbum(a);
                user.getAlbums().get(albumIndex).add(p);
                updateAlbums();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void updateAlbums() {
        try {
            AlbumsData.writeAlbums(user, user.getContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return name.toLowerCase().equals(album.name.toLowerCase());
    }

    @Override
    public String toString() {
        return name;
    }
}

package edu.rutgers.model;

import android.content.Context;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {
    static final long serialVersionUID = 2L;
    private List<Album> albums;
    transient private Context context;

    public User(Context context) {
        this.context = context;
        albums = new ArrayList<>();
    }

    public List<Album> getAlbums() throws IOException {
        return albums;
    }

    public void create(Album a) {
        if (!albums.contains(a)) {
            albums.add(a);
            try {
                AlbumsData.writeAlbums(this, context);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void delete(Album a) {
        if (albums.contains(a)) {
            for (Photo p : a.getPhotos()) {
                a.delete(p);
            }
            albums.remove(a);
            try {
                AlbumsData.writeAlbums(this, context);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void rename(Album a, String name) {
        int newAlbum = albums.indexOf(new Album(name, this));
        if (!albums.contains(newAlbum)) {
            albums.get(albums.indexOf(a)).setName(name);
            try {
                AlbumsData.writeAlbums(this, context);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}

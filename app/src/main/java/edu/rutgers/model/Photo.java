package edu.rutgers.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Photo implements Serializable {
    static final long serialVersionUID = 4L;

    private final File file;

    private Album album;

    private final List<Tag> tags;

    public Photo(File file, Album album) {
        this.file = file;
        this.album = album;
        this.tags = new ArrayList<>();
    }

    public String getName() {
        String fileName = file.getName();
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        }
    }

    public File getFile() {
        return file;
    }

    public void addTag(Tag newTag) {
        if (newTag.getKey().equals("location")) {
            for (Tag t : tags) {
                if (t.getKey().equals("location") && !t.equals(newTag)) {
                    t.setValue(newTag.getValue());
                    album.updateAlbums();
                    return;
                }
            }
        }
        if (!tags.contains(newTag)) {
            tags.add(newTag);
            album.updateAlbums();
        }
    }

    public void removeTag(Tag t) {
        if (tags.contains(t)) {
            tags.remove(t);
            album.updateAlbums();
        }
    }

    public List<Tag> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return getName();
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return file.equals(photo.file);
    }
}

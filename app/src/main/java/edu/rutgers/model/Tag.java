package edu.rutgers.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Tag implements Comparable<Tag>, Serializable {
    static final long serialVersionUID = 5L;

    private final String key;

    private String value;

    public static List<String> types = Arrays.asList("location" , "person");

    public Tag(String key, String value) {
        this.key = key.trim();
        this.value = value.trim();
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return key + "=" + value;
    }

    @Override
    public int compareTo(Tag t) {
        return this.toString().toLowerCase().compareTo(t.toString().toLowerCase());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return compareTo(tag) == 0;
    }

    public static List<String> getTagTypes() {
        return types;
    }
}

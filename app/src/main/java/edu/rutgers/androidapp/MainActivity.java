package edu.rutgers.androidapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.NavController;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import edu.rutgers.androidapp.databinding.ActivityMainBinding;
import edu.rutgers.view.UserViewModel;
import edu.rutgers.view.photo.PhotoFragment;
import edu.rutgers.view.photos.PhotosFragment;
import edu.rutgers.view.search.SearchFragment;
import edu.rutgers.view.search.SearchOption;
import edu.rutgers.view.search.SearchOptionFragment;


public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding bind;
    private UserViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UserViewModel.class);
        bind = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(bind.getRoot());
        //BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBar = new AppBarConfiguration.Builder(R.id.navigation_albums,
                R.id.navigation_create, R.id.navigation_delete, R.id.navigation_rename, R.id.navigation_search).build();
        NavController navCtrl = Navigation.findNavController(this, R.id.nav_main);
        NavigationUI.setupActionBarWithNavController(this, navCtrl, appBar);
        NavigationUI.setupWithNavController(bind.navView, navCtrl);
    }
    public UserViewModel getViewModel(){
        return viewModel;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Fragment navFragment = this.getSupportFragmentManager().findFragmentById(R.id.nav_main);
        Fragment mapFragment = navFragment.getChildFragmentManager().getPrimaryNavigationFragment();
        NavController navc = Navigation.findNavController(mapFragment.getView());

        if (mapFragment instanceof PhotoFragment) {
            viewModel.setSelPhoto(null);
        }

        if (mapFragment instanceof PhotosFragment) {
            viewModel.setSelAlbum(null);
            BottomNavigationView bottomNavigationView = this.findViewById(R.id.nav_view);
            bottomNavigationView.setVisibility(View.VISIBLE);
        }

        if (mapFragment instanceof SearchOptionFragment) {
            if (viewModel.getSearchOption() == SearchOption.PEOPLE) {
                viewModel.setSearchOption(SearchOption.MAIN);
            } else if (viewModel.getSearchOption() == SearchOption.MULTIPEOPLE) {
                viewModel.setSearchOption(SearchOption.PEOPLE);
            }
        }

        navc.popBackStack();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Fragment f = this.getSupportFragmentManager().findFragmentById(R.id.navigation_photo);
        if(f instanceof PhotoFragment)
            // do something with f
            Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();

    }

}
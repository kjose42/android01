package edu.rutgers.view.rename;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentRenameBinding;
import edu.rutgers.model.Album;
import edu.rutgers.view.UserViewModel;

public class RenameFragment extends Fragment {
    private FragmentRenameBinding bind;
    private Button renameButton;
    private EditText albumName;
    private ListView list;
    private UserViewModel viewModel;
    private ArrayAdapter adapt;
    private Album selAlbum;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        bind = FragmentRenameBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        adapt = new ArrayAdapter<Album>(getActivity(), R.layout.activity_list,
                viewModel.getAlbums());
        list = (ListView) root.findViewById(R.id.list_fxml);
        list.setOnItemClickListener(listSelect);
        list.setAdapter(adapt);
        renameButton = root.findViewById(R.id.renameButton);
        albumName = root.findViewById(R.id.albumName);
        UserViewModel viewModel = ((MainActivity)getActivity()).getViewModel();
        renameButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(selAlbum != null){
                    viewModel.getUser().rename(selAlbum, albumName.getText().toString());
                    adapt.notifyDataSetChanged();}
            }
        });
        return root;
    }
    private AdapterView.OnItemClickListener listSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            selAlbum = (Album)list.getItemAtPosition(position);
        }
    };
}

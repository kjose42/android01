package edu.rutgers.view.photos;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.databinding.FragmentPhotosBinding;
import edu.rutgers.model.Album;
import edu.rutgers.model.Photo;
import edu.rutgers.view.CustomAdapter;
import edu.rutgers.view.UserViewModel;
import edu.rutgers.androidapp.R;

public class PhotosFragment extends Fragment{
    private FragmentPhotosBinding bind;
    private UserViewModel viewModel;
    private BottomNavigationView bottomNavigationView;
    private Button addButton;
    private Button slidesButton;
    private Album album;
    private NavController navc = null;
    private Photo selPhoto = null;
    private CustomAdapter adapt;
    private ListView list;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        album = viewModel.getSelAlbum();
        bind = FragmentPhotosBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        adapt = new CustomAdapter(viewModel.getSelAlbum().getPhotos(), (MainActivity)getContext());
        bottomNavigationView = (getActivity()).findViewById(R.id.nav_view);
        bottomNavigationView.setVisibility(View.INVISIBLE);

        addButton = root.findViewById(R.id.addButton);
        slidesButton = root.findViewById(R.id.slidesButton);

        list = root.findViewById(R.id.list_fxml);
        list.setOnItemClickListener(listSelect);
        list.setAdapter(adapt);

        ActivityResultLauncher<Intent> mGetContent = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        Uri photoURI = intent.getData();

                        String filename = photoURI.getLastPathSegment() + "." + getContext().getContentResolver().getType(photoURI).split("/")[1];;
                        File destination = new File((getContext()).getFilesDir() + File.separator + filename);

                        if (!destination.exists()) {
                            try {
                                FileInputStream srcStream = (FileInputStream) getContext().getContentResolver().openInputStream(photoURI);
                                FileChannel in = srcStream.getChannel();
                                FileChannel out = new FileOutputStream(destination).getChannel();

                                try {
                                    out.transferFrom(in, 0, in.size());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } finally {
                                    if (in != null)
                                        in.close();
                                    if (out != null)
                                        out.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "No File :(", Toast.LENGTH_SHORT).show();
                            }
                            Photo photo = new Photo(destination, album);
                            album.add(photo);
                            //test.setImageURI(photoURI);
                            adapt.notifyDataSetChanged();
                        }
                    }
                }
        });

        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                mGetContent.launch(intent);
            }
        });
        slidesButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                navc.navigate(R.id.action_navigation_photos_to_navigation_slides);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(album.getName());
        navc = Navigation.findNavController(view);
    }

    private AdapterView.OnItemClickListener listSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            selPhoto = (Photo)list.getItemAtPosition(position);
            viewModel.setSelPhoto(selPhoto);
            navc.navigate(R.id.action_navigation_photos_to_navigation_photo);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind = null;
    }

    @SuppressLint("Range")
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
}



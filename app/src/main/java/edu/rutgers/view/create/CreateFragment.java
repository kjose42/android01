package edu.rutgers.view.create;

import edu.rutgers.androidapp.R;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;

import edu.rutgers.androidapp.databinding.FragmentCreateBinding;
import edu.rutgers.model.Album;
import edu.rutgers.view.UserViewModel;
import edu.rutgers.androidapp.MainActivity;

public class CreateFragment extends Fragment {
    private Button createButton;
    private EditText albumName;
    private FragmentCreateBinding bind;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bind = FragmentCreateBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        createButton = root.findViewById(R.id.createButton);
        albumName = root.findViewById(R.id.albumName);
        UserViewModel viewModel = ((MainActivity)getActivity()).getViewModel();
        createButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (albumName.getText().toString().trim().equals("")) return;
                Album newAlbum = new Album(albumName.getText().toString(), viewModel.getUser());
                viewModel.getUser().create(newAlbum);
            }
                                       });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind = null;
    }
}

package edu.rutgers.view.search;

public enum SearchType {
    LOCATION,
    PERSON,
    PEOPLE_AND,
    PEOPLE_OR
}

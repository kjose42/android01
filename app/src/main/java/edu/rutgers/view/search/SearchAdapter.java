package edu.rutgers.view.search;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import edu.rutgers.androidapp.R;
import edu.rutgers.model.Photo;
import edu.rutgers.model.Tag;

public class SearchAdapter extends ArrayAdapter<Photo> {
    Context context;
    int resource, textViewResourceId;
    List<Photo> photos, tempPhotos, suggestions;
    private String search;
    private SearchType searchType;

    public SearchAdapter(@NonNull Context context, int resource, int textViewResourceId, List<Photo> photos, String search, SearchType searchType) {
        super(context, resource, textViewResourceId, photos);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.photos = photos;
        this.search = search;
        this.searchType = searchType;

        tempPhotos = new ArrayList<>(photos);
        suggestions = new ArrayList<>();
    }

    @Override
    public View getView(int i, View v, ViewGroup viewGroup) {
        View view = v;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_layout, null);
        }
        ImageView imgView = view.findViewById(R.id.img_view);
        Bitmap bitmap = BitmapFactory.decodeFile(photos.get(i).getFile().getAbsolutePath());

        imgView.setImageBitmap(bitmap);
        return view;
    }

    @Override
    public Filter getFilter() {
        return photoFilter;
    }

    Filter photoFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = "";
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                if (searchType != SearchType.PEOPLE_AND && searchType != SearchType.PEOPLE_OR) {
                    for (Photo p : tempPhotos) {
                        for (Tag t : p.getTags()) {
                            if (t.getKey().equals(search) && t.getValue().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                suggestions.add(p);
                                break;
                            }
                        }
                    }
                } else {
                    String[] constraintString = constraint.toString().toLowerCase().trim().split(",", 2);
                    if (constraintString.length == 2
                            && !constraintString[0].trim().equals("")
                            && !constraintString[1].trim().equals("")) {
                        String person1 = constraintString[0];
                        String person2 = constraintString[1];

                        Predicate<Photo> tag1Matches =
                                photo -> photo.getTags()
                                        .stream()
                                        .anyMatch(tag -> tag.getValue().toLowerCase().startsWith(person1));
                        Predicate<Photo> tag2Matches = photo -> photo.getTags()
                                        .stream()
                                        .anyMatch(tag -> tag.getValue().toLowerCase().startsWith(person2));

                        if (searchType == SearchType.PEOPLE_AND) {
                            suggestions.addAll(photos.stream()
                                    .filter(tag1Matches.and(tag2Matches))
                                    .collect(Collectors.toList()));
                        } else {
                            suggestions.addAll(photos.stream()
                                    .filter(tag1Matches.or(tag2Matches))
                                    .collect(Collectors.toList()));
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Photo> filterList = (ArrayList<Photo>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Photo p : filterList) {
                    add(p);
                    notifyDataSetChanged();
                }
            }
        }
    };

}

package edu.rutgers.view.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentSearchOptionBinding;
import edu.rutgers.view.UserViewModel;

public class SearchOptionFragment extends Fragment {
    private UserViewModel viewModel;
    private FragmentSearchOptionBinding bind;
    private NavController navc = null;

    private TextView text1;
    private TextView text2;
    private ImageView image1;
    private ImageView image2;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        bind = FragmentSearchOptionBinding.inflate(inflater, container, false);
        View root = bind.getRoot();

        viewModel.setSearchType(null);

        text1 = root.findViewById(R.id.text1);
        text2 = root.findViewById(R.id.text2);
        image1 = root.findViewById(R.id.image1);
        image2 = root.findViewById(R.id.image2);

        BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.nav_view);

        if (viewModel.getSearchOption() == SearchOption.MAIN) {
            ((MainActivity)getActivity()).getSupportActionBar().setTitle("Search");
            ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            image1.setImageResource(R.drawable.android_location);
            image2.setImageResource(R.drawable.android_people);
            text1.setText("Location");
            text2.setText("People");
            bottomNavigationView.setVisibility(View.VISIBLE);
        } else {
            bottomNavigationView.setVisibility(View.INVISIBLE);
        }

        if (viewModel.getSearchOption() == SearchOption.PEOPLE) {
            ((MainActivity)getActivity()).getSupportActionBar().setTitle("People Search");
            image1.setImageResource(R.drawable.android_people);
            image2.setImageResource(R.drawable.android_two_people);
            text1.setText("One Person");
            text2.setText("Two People");
        }

        if (viewModel.getSearchOption() == SearchOption.MULTIPEOPLE) {
            ((MainActivity)getActivity()).getSupportActionBar().setTitle("Multiple Search");
            image1.setImageResource(R.drawable.android_two_people_and);
            image2.setImageResource(R.drawable.android_two_people_or);
            text1.setText("Both People");
            text2.setText("One or the Other");
        }

        image1.setOnClickListener(image1Select);
        image2.setOnClickListener(image2Select);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navc = Navigation.findNavController(view);
    }

    private AdapterView.OnClickListener image1Select = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            if (viewModel.getSearchOption() == SearchOption.MAIN) {
                viewModel.setSearchType(SearchType.LOCATION);
            } else if (viewModel.getSearchOption() == SearchOption.PEOPLE) {
                viewModel.setSearchType(SearchType.PERSON);
            } else {
                viewModel.setSearchType(SearchType.PEOPLE_AND);
            }
            navc.navigate(R.id.action_navigation_search_to_navigation_search);
        }
    };

    private AdapterView.OnClickListener image2Select = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            if (viewModel.getSearchOption() == SearchOption.MAIN) {
                viewModel.setSearchOption(SearchOption.PEOPLE);
                navc.navigate(R.id.action_navigation_search_option_self);
            } else if (viewModel.getSearchOption() == SearchOption.PEOPLE) {
                viewModel.setSearchOption(SearchOption.MULTIPEOPLE);
                navc.navigate(R.id.action_navigation_search_option_self);
            } else {
                viewModel.setSearchType(SearchType.PEOPLE_OR);
                navc.navigate(R.id.action_navigation_search_to_navigation_search);
            }
        }
    };


}

package edu.rutgers.view.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentSearchBinding;
import edu.rutgers.model.Photo;
import edu.rutgers.view.UserViewModel;

public class SearchFragment extends Fragment {
    private UserViewModel viewModel;
    private FragmentSearchBinding bind;
    private NavController navc = null;
    private AutoCompleteTextView textView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        viewModel.setSelAlbum(null);

        bind = FragmentSearchBinding.inflate(inflater, container, false);
        View root = bind.getRoot();

        BottomNavigationView bottomNavigationView = (getActivity()).findViewById(R.id.nav_view);
        bottomNavigationView.setVisibility(View.INVISIBLE);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView = root.findViewById(R.id.search);

        List<Photo> photos = viewModel.getAllPhotos();

        textView.setOnItemClickListener(photoSelect);

        SearchType searchType = null;
        String search = "";

        switch (viewModel.getSearchType()) {
            case LOCATION:
                searchType = SearchType.LOCATION;
                ((MainActivity)getActivity()).getSupportActionBar().setTitle("Location Search");
                search = "location";
                break;
            case PERSON:
                searchType = SearchType.PERSON;
                ((MainActivity)getActivity()).getSupportActionBar().setTitle("Person Search");
                search = "person";
                break;
            case PEOPLE_AND:
                searchType = SearchType.PEOPLE_AND;
                ((MainActivity)getActivity()).getSupportActionBar().setTitle("People AND Search");
                textView.setHint("Person1,Person2");
                break;
            case PEOPLE_OR:
                searchType = SearchType.PEOPLE_OR;
                ((MainActivity)getActivity()).getSupportActionBar().setTitle("People OR Search");
                textView.setHint("Person1,Person2");
                break;
        }

        SearchAdapter adapter = new SearchAdapter(getContext(), R.layout.activity_list, R.id.img_view, photos, search, searchType);
        textView.setAdapter(adapter);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navc = Navigation.findNavController(view);
    }

    private AdapterView.OnItemClickListener photoSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            Photo selPhoto = (Photo)adaptView.getItemAtPosition(position);
            viewModel.setSelPhoto(selPhoto);
            viewModel.setSelAlbum(selPhoto.getAlbum());
            navc.navigate(R.id.action_navigation_search_to_navigation_photo);
        }
    };
}

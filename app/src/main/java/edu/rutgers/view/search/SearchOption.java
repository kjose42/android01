package edu.rutgers.view.search;

public enum SearchOption {
    MAIN,
    PEOPLE,
    MULTIPEOPLE
}

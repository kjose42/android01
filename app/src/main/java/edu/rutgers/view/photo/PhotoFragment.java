package edu.rutgers.view.photo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.InputType;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import java.util.List;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;

import edu.rutgers.androidapp.databinding.FragmentPhotoBinding;
import edu.rutgers.model.Album;
import edu.rutgers.model.Photo;
import edu.rutgers.model.Tag;
import edu.rutgers.view.UserViewModel;

public class PhotoFragment extends Fragment{
    private NavController navc = null;
    private UserViewModel viewModel;
    private FragmentPhotoBinding bind;
    private ListView list;
    private ArrayAdapter adapt;

    private Photo photo;
    private Tag selTag;

    private Button addTagButton;
    private Button removeTagButton;
    private Button moveButton;
    private Button removeButton;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bind = FragmentPhotoBinding.inflate(inflater, container, false);
        View root = bind.getRoot();

        viewModel = ((MainActivity)getActivity()).getViewModel();

        photo = viewModel.getSelPhoto();

        adapt = new ArrayAdapter<>(getActivity(), R.layout.activity_list,
                photo.getTags());

        list = root.findViewById(R.id.list_fxml);
        list.setOnItemClickListener(listSelect);
        list.setAdapter(adapt);

        ImageView photoView = (ImageView) root.findViewById(R.id.imageView);

        Bitmap bitmap = BitmapFactory.decodeFile(photo.getFile().getAbsolutePath());
        photoView.setImageBitmap(bitmap);

        addTagButton = root.findViewById(R.id.addTagButton);
        addTagButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Enter new tag:");

                final EditText keyText = new EditText(getContext());
                keyText.setHint("key");
                keyText.setInputType(InputType.TYPE_CLASS_TEXT);

                final EditText valueText = new EditText(getContext());
                valueText.setHint("value");
                valueText.setInputType(InputType.TYPE_CLASS_TEXT);

                LinearLayout lay = new LinearLayout(getContext());
                lay.setOrientation(LinearLayout.VERTICAL);
                lay.addView(keyText);
                lay.addView(valueText);
                builder.setView(lay);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String key = keyText.getText().toString().trim().toLowerCase();
                        String value = valueText.getText().toString().trim();

                        List<String> tagTypes = Tag.getTagTypes();
                        if (!tagTypes.contains(key)) {
                            Toast.makeText(getContext(), "Tag Keys can only be location or person", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (value.equals("")) {
                            Toast.makeText(getContext(), "Please enter a Value for your Tag", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (value.contains(",")) {
                            Toast.makeText(getContext(), "Cannot have commas in your name", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Tag newTag = new Tag(key, value);

                        photo.addTag(newTag);
                        adapt.notifyDataSetChanged();
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        removeTagButton = root.findViewById(R.id.removeTagButton);
        removeTagButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (selTag != null) {
                    new AlertDialog.Builder((MainActivity) getActivity())
                            .setTitle("Confirm Deletion")
                            .setMessage("Do you really want to delete " + selTag + "?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    photo.removeTag(selTag);
                                    selTag = null;
                                    list.clearChoices();
                                    list.setAdapter(adapt);
                                    adapt.notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                } else if (photo.getTags().size() > 0){
                    Toast.makeText(getContext(), "Please Select a Tag to Delete", Toast.LENGTH_SHORT).show();
                }
            }
        });

        removeButton = root.findViewById(R.id.removeButton);
        removeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                new AlertDialog.Builder(getActivity())
                        .setTitle("Confirm Deletion")
                        .setMessage("Do you really want to delete this photo?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                viewModel.getSelAlbum().delete(photo);
                                viewModel.setSelPhoto(null);
                                navc.popBackStack();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
        moveButton = root.findViewById(R.id.moveButton);
        moveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                navc.navigate(R.id.action_navigation_photo_to_navigation_move);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(photo.getAlbum().getName());
        navc = Navigation.findNavController(view);
    }

    private AdapterView.OnItemClickListener listSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            selTag = (Tag)list.getItemAtPosition(position);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind = null;
    }
}



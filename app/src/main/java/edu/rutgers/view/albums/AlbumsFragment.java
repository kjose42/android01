package edu.rutgers.view.albums;

import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentAlbumsBinding;
import edu.rutgers.model.Album;
import edu.rutgers.view.UserViewModel;

public class AlbumsFragment extends Fragment{
    private FragmentAlbumsBinding bind;
    private ArrayAdapter adapt;
    private Album selAlbum;
    private ListView list;
    private NavController navc = null;
    private UserViewModel viewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        bind = FragmentAlbumsBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        adapt = new ArrayAdapter<>(getActivity(), R.layout.activity_list,
                viewModel.getAlbums());
        list = (ListView) root.findViewById(R.id.list_fxml);
        list.setOnItemClickListener(listSelect);
        list.setAdapter(adapt);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navc = Navigation.findNavController(view);
        navc.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController navController, @NonNull NavDestination navDestination, @Nullable Bundle bundle) {
                if (navDestination.getId() == R.id.navigation_photos) {
                    if (selAlbum != null) {
                        navDestination.setLabel(selAlbum.getName());
                    }
                }
            }
        });
    }

    private AdapterView.OnItemClickListener listSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            selAlbum = (Album)list.getItemAtPosition(position);
            viewModel.setSelAlbum(selAlbum);

            navc.navigate(R.id.action_navigation_albums_to_navigation_photos);
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind = null;
    }
}


package edu.rutgers.view;

import static edu.rutgers.view.search.SearchOption.MAIN;

import android.app.Application;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.LiveData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.rutgers.androidapp.R;
import edu.rutgers.model.Album;
import edu.rutgers.model.AlbumsData;
import edu.rutgers.model.Photo;
import edu.rutgers.model.User;
import edu.rutgers.view.search.SearchOption;
import edu.rutgers.view.search.SearchType;


public class UserViewModel extends AndroidViewModel {
    Application app;
    private final MutableLiveData<String> mText;
    private User user;
    private Album selAlbum;
    private Photo selPhoto;
    private SearchOption searchOption = null;
    private SearchType searchType = null;

    public UserViewModel(Application app){
        super(app);
        this.app = app;
        this.selAlbum = null;
        this.searchOption = MAIN;
        try {
            user = AlbumsData.getAlbums(app.getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mText = new MutableLiveData<>();
        mText.setValue("albums");
    }

    public List<Album> getAlbums(){
        try {
            return user.getAlbums();
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<Photo> getAllPhotos() {
        List<Photo> allPhotos = new ArrayList<>();
        try {
            for (Album a : user.getAlbums()) {
                allPhotos.addAll(a.getPhotos());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allPhotos;
    }

    public Album getSelAlbum() {
        return selAlbum;
    }

    public void setSelAlbum(Album selAlbum) {
        this.selAlbum = selAlbum;
    }

    public Photo getSelPhoto() {
        return selPhoto;
    }

    public void setSelPhoto(Photo selPhoto) {
        this.selPhoto = selPhoto;
    }

    public User getUser(){
        return user;
    }

    public SearchOption getSearchOption() {
        return searchOption;
    }

    public void setSearchOption(SearchOption searchOption) {
        this.searchOption = searchOption;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public LiveData<String> getText(){
        return mText;
    }
}
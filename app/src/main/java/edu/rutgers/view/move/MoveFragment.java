package edu.rutgers.view.move;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import java.util.stream.Collectors;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentMoveBinding;
import edu.rutgers.model.Album;
import edu.rutgers.model.Photo;
import edu.rutgers.view.CustomAdapter;
import edu.rutgers.view.UserViewModel;

public class MoveFragment extends Fragment {
    private FragmentMoveBinding bind;
    private UserViewModel viewModel;
    private ArrayAdapter adapt;
    private ListView list;
    private Album selAlbum;
    private Album moveAlbum = null;
    private Photo selPhoto;
    private Button moveButton;
    private NavController navc;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        bind = FragmentMoveBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        selAlbum = viewModel.getSelAlbum();
        selPhoto = viewModel.getSelPhoto();
        adapt = new ArrayAdapter<>(getActivity(), R.layout.activity_list,
                viewModel.getAlbums().stream().filter(album -> !album.equals(selAlbum)).collect(Collectors.toList()));
        list = (ListView) root.findViewById(R.id.list_fxml);
        list.setOnItemClickListener(listSelect);
        list.setAdapter(adapt);
        moveButton = root.findViewById(R.id.moveButton);
        moveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (moveAlbum != null) {
                    selAlbum.move(selPhoto, moveAlbum);
                    navc.popBackStack();
                }
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navc = Navigation.findNavController(view);
    }

    private AdapterView.OnItemClickListener listSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            moveAlbum = (Album)list.getItemAtPosition(position);
        }
    };
}

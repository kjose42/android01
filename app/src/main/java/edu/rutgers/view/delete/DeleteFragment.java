package edu.rutgers.view.delete;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentDeleteBinding;
import edu.rutgers.model.Album;
import edu.rutgers.view.UserViewModel;

public class DeleteFragment extends Fragment {
    private FragmentDeleteBinding bind;
    private Button deleteButton;
    private ListView list;
    private UserViewModel viewModel;
    private ArrayAdapter adapt;
    private Album selAlbum;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity)getActivity()).getViewModel();
        bind = FragmentDeleteBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        adapt = new ArrayAdapter<Album>(getActivity(), R.layout.activity_list,
                viewModel.getAlbums());
        list = (ListView) root.findViewById(R.id.list_fxml);
        list.setOnItemClickListener(listSelect);
        list.setAdapter(adapt);
        deleteButton = root.findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(selAlbum != null) {
                    new AlertDialog.Builder((MainActivity) getActivity())
                        .setTitle("Confirm Deletion")
                        .setMessage("Do you really want to delete " + selAlbum.getName() + "?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                viewModel.getUser().delete(selAlbum);
                                selAlbum = null;
                                list.clearChoices();
                                list.setAdapter(adapt);
                                adapt.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                }
            }
        });
        return root;
    }

    private AdapterView.OnItemClickListener listSelect = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView adaptView, View v, int position, long id){
            selAlbum = (Album)list.getItemAtPosition(position);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind = null;
    }
}

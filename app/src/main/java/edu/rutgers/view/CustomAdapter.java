package edu.rutgers.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;

import edu.rutgers.androidapp.R;
import edu.rutgers.model.Photo;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.List;

public class CustomAdapter extends BaseAdapter implements ListAdapter {

    private List<Photo> items;
    private Context context;
    public CustomAdapter(List<Photo> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View v, ViewGroup viewGroup) {
        View view = v;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_layout, null);
        }
        ImageView imgView = view.findViewById(R.id.img_view);
        Bitmap bitmap = BitmapFactory.decodeFile(items.get(i).getFile().getAbsolutePath());
        imgView.setImageBitmap(bitmap);
        return view;
    }
}

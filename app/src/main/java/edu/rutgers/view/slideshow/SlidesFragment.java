package edu.rutgers.view.slideshow;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import edu.rutgers.androidapp.MainActivity;
import edu.rutgers.androidapp.R;
import edu.rutgers.androidapp.databinding.FragmentSlidesBinding;
import edu.rutgers.model.Album;
import edu.rutgers.view.UserViewModel;

public class SlidesFragment extends Fragment {
    private FragmentSlidesBinding bind;
    private UserViewModel viewModel;
    private Album album;
    private Button nextButton;
    private Button backButton;
    private ImageView photoView;
    private int slidePos = 0;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ((MainActivity) getActivity()).getViewModel();
        album = viewModel.getSelAlbum();
        bind = FragmentSlidesBinding.inflate(inflater, container, false);
        View root = bind.getRoot();
        photoView = (ImageView) root.findViewById(R.id.imageView);
        System.out.println(album.getPhotos().get(slidePos));
        Bitmap bitmap = BitmapFactory.decodeFile(album.getPhotos().get(slidePos).getFile().getAbsolutePath());
        photoView.setImageBitmap(bitmap);
        nextButton = root.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(slidePos < album.getPhotos().size() - 1){
                    slidePos = slidePos + 1;
                    Bitmap bitmap = BitmapFactory.decodeFile(album.getPhotos().get(slidePos).getFile().getAbsolutePath());
                    photoView.setImageBitmap(bitmap); }
            }
        });
        backButton = root.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(slidePos > 0){
                    slidePos = slidePos - 1;
                    Bitmap bitmap = BitmapFactory.decodeFile(album.getPhotos().get(slidePos).getFile().getAbsolutePath());
                    photoView.setImageBitmap(bitmap);}
            }
        });
        return root;
    }
}


